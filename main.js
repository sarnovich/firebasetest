
let objetivo;
const lista2 = document.getElementById("lista-tareas2")
let updateBtn = document.getElementById('update')
let newTitle = null
let objetcId = null

const form = document.getElementById("add-tarea-form")
form.addEventListener('submit', e => {
    e.preventDefault();
    db.collection('tareas').add({
        titulo: form.titulo.value
    });

    form.titulo.value = ''
})


const renderList = (doc) => {
    let li = document.createElement('li');
    li.className = 'collection-item';
    li.setAttribute('data-id', doc.id);

    let div = document.createElement('div')
    let titulo = document.createElement('span');
    titulo.className = "spaaan";
    titulo.textContent = doc.data().titulo

    let deleteBtn = document.createElement('i');
    deleteBtn.className = 'material-icons secondary-content'
    deleteBtn.innerText ='delete';
    deleteBtn.addEventListener('click', e => {
        objetivo = e.target.parentElement.parentElement.getAttribute('data-id')
        db.collection('tareas').doc(objetivo).delete()

    })

    let link = document.createElement('a')
    link.href = "#modal1";
    link.className = 'modal-trigger secondary-content'

    let editBtn = document.createElement('i')
    editBtn.className = "material-icons"
    editBtn.innerText = "edit"
    link.appendChild(editBtn)
    div.appendChild(titulo)
    div.appendChild(deleteBtn)
    div.appendChild(link)
    li.appendChild(div)
    
    editBtn.addEventListener('click', e => {
        objetcId = e.target.parentElement.parentElement.parentElement.getAttribute('data-id')
        
    });

    lista2.appendChild(li)

}

updateBtn.addEventListener ('click', e => {
    newTitle = document.getElementsByName('newTitle')[0].value;
    db.collection('tareas').doc(objetcId).update({
        titulo: newTitle
    })

})

db.collection('tareas').orderBy('titulo').onSnapshot( snapshot => {
    let cambos = snapshot.docChanges()
    cambos.forEach(cambio => {
        console.log(cambio.type)
        if(cambio.type == 'added') {
            renderList(cambio.doc);
            console.log(cambio.doc.data()); } 
            else if ( cambio.type =='removed') { 
                console.log("eliminado")
                let li = document.querySelector(`[data-id=${objetivo}]`)

                console.log(li)
                lista2.removeChild(li)
            }else if (cambio.type == 'modified') {
                console.log("modificado")
                let li = document.querySelector(`[data-id=${objetcId}]`)
                console.log(li)
                li.getElementsByClassName("spaaan")[0].textContent = newTitle
                newTitle = ''
            }

        
        
    });
})
